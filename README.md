# Exemple de fichier readme

Il vous faudra des infos pour comprendre mon projet bizarre.

![](https://www.referenseo.com/wp-content/uploads/2019/03/image-attractive-960x540.jpg)
## Explications

```python
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('phenomena')
```
